package runners;


import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeSuite;
import support.cucumber.BaseRunner;

@CucumberOptions(
        tags = ""
)

public class Runner extends BaseRunner {

    @BeforeSuite
    public void setUp() {
        browser = "chrome";
        environment = "stg";
        headless = true;
    }
}
