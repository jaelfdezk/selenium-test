package pages.base;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;
import support.selenium.DriverManager;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class BasePage {
    private static WebDriver driver;

    public static void setDriver(WebDriver webDriver) {
        driver = webDriver;
    }

    protected WebDriver getDriver(){
        return driver;
    }

    public static int getImplicitlyTime(){
        return (int) driver.manage().timeouts().getImplicitWaitTimeout().toSeconds();
    }

    public BasePage() {
        PageFactory.initElements(driver, this);
    }

    protected String getEnvironment(){
        return DriverManager.getEnvironment();
    }

    protected WebElement findElement(WebElement element, By locator) {
        return element.findElement(locator);
    }

    protected void enter(WebElement element, String text) {
        waitToBeClickable(element);
        element.click();
        element.clear();
        element.sendKeys(text);
    }

    protected boolean isEnabled(WebElement element) {
        return element.isEnabled();
    }

    public void back() {
        getDriver().navigate().back();
    }

    public void goTo(String url) {
        getDriver().get(url);
    }

    protected void click(WebElement element) {
        waitToBeClickable(element);
        element.click();
    }

    protected void click(WebElement element, boolean waitToBeClickable) {
        if (waitToBeClickable) {
            click(element);
        } else {
            element.click();
        }
    }

    protected void click(WebElement element, By locator) {
        click(findElement(element, locator));
    }

    protected void clickUntilDisplayElement(WebElement elementToClick, WebElement elementToBeDisplayed) {
        clickUntilDisplayElement(elementToClick, elementToBeDisplayed, 10, 1);
    }

    protected void clickUntilDisplayElement(WebElement elementToClick, WebElement elementToBeDisplayed, int retries, int secondsToWait) {
        for (int i = 0; i < retries; i++) {
            try {
                if (isDisplayed(elementToBeDisplayed, secondsToWait)) {
                    break;
                }else {
                    click(elementToClick);
                }
            } catch (ElementClickInterceptedException | StaleElementReferenceException ignored) {
            }
        }
    }

    protected void clickJs(WebElement element) {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", element);
    }

    protected void doubleClick(WebElement element) {
        Actions actions = new Actions(getDriver());
        actions.doubleClick(element).perform();
    }

    public void pressEscape() {
        Actions action = new Actions(getDriver());
        action.sendKeys(Keys.ESCAPE).perform();
    }

    protected String getText(WebElement element) {
        waitToBeVisibility(element);
        return element.getText();
    }

    protected String getText(WebElement element, By locator) {
        waitToBeVisibility(element);
        return element.findElement(locator).getText();
    }

    protected String getText(WebElement element, boolean beVisible) {
        if (beVisible) {
            return getText(element);
        } else {
            return element.getText();
        }
    }

    protected String getTextIgnoringChild(WebElement element) {
        waitToBeVisibility(element);
        String text = element.getText().trim();
        setImplicitlyWaitTime(1);
        List<WebElement> children = element.findElements(By.xpath("./*"));
        if (!children.isEmpty()) {
            for (WebElement child : children) {
                text = text.replaceFirst(child.getText(), "").trim();
            }
        }
        setImplicitlyWaitTime(10);
        return text;
    }

    protected String moveToElementToGetText(WebElement element) {
        waitToBeVisibility(element);
        moveToElement(element);
        return element.getText();
    }

    protected String moveToElementToGetText(WebElement element, By locator) {
        element = findElement(element, locator);
        return moveToElementToGetText(element);
    }

    protected String getValue(WebElement element) {
        waitToBeVisibility(element);
        return element.getAttribute("value");
    }

    protected String getAttribute(WebElement element, String attribute) {
        waitToBeVisibility(element);
        moveToElement(element);
        return element.getAttribute(attribute);
    }

    protected boolean isDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected boolean isDisplayed(WebElement element, int secondsToWait) {
        setImplicitlyWaitTime(secondsToWait);
        try {
            boolean isElementDisplayed = element.isDisplayed();
            setImplicitlyWaitTime(10);
            return isElementDisplayed;
        } catch (NoSuchElementException e) {
            setImplicitlyWaitTime(10);
            return false;
        }
    }

    protected boolean isDisplayed(WebElement element, By locator) {
        try {
            return isDisplayed(findElement(element, locator));
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected boolean isDisplayed(WebElement element, By locator, int secondsToWait) {
        setImplicitlyWaitTime(secondsToWait);
        try{
            return isDisplayed(findElement(element, locator), secondsToWait);
        } catch (NoSuchElementException e) {
            setImplicitlyWaitTime(10);
            return false;
        }
    }

    public static void setImplicitlyWaitTime(int seconds) {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(seconds));
    }

    protected void moveToElement(WebElement element) {
        Actions actions = new Actions(getDriver());
        actions.moveToElement(element);
        actions.release().perform();
    }

    // Waits
    private Wait<WebDriver> getWait() {
        return new FluentWait<>(getDriver())
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofMillis(100))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class);
    }

    protected void waitToBeClickable(WebElement element) {
        getWait().until(ExpectedConditions.elementToBeClickable(element));
    }

    protected void waitToBeVisibility(WebElement element) {
        getWait().until(ExpectedConditions.visibilityOf(element));
    }

    protected WebElement findElementInAListByContainsText(
            List<WebElement> elementList, String textToSearch) {
        try {
            return elementList.stream()
                    .filter(element -> element.getText().contains(textToSearch))
                    .findFirst()
                    .get();
        } catch (java.util.NoSuchElementException exception) {
            return null;
        } catch (StaleElementReferenceException exception) {
            return findElementInAListByContainsText(elementList, textToSearch);
        }
    }

    protected WebElement findElementInAListByContainsText(List<WebElement> elementList, By locator, String textToSearch) {
        try {
            return elementList.stream()
                    .filter(element -> element.findElement(locator).getText().contains(textToSearch))
                    .findFirst()
                    .get();
        } catch (java.util.NoSuchElementException exception) {
            return null;
        } catch (StaleElementReferenceException exception) {
            return findElementInAListByContainsText(elementList, textToSearch);
        }
    }

    protected WebElement findElementInAListByAttributeContainsText(
            List<WebElement> elementList, By locator, String attribute, String textToSearch) {
        try {
            return elementList.stream()
                    .filter(element -> element.findElement(locator).getAttribute(attribute).contains(textToSearch))
                    .findFirst()
                    .get();
        } catch (java.util.NoSuchElementException exception) {
            return null;
        } catch (StaleElementReferenceException exception) {
            return findElementInAListByContainsText(elementList, textToSearch);
        }
    }

    protected void waitUntilElementDisappear(WebElement element) {
        setImplicitlyWaitTime(1);
        new WebDriverWait(getDriver(), Duration.ofSeconds(10)).until(ExpectedConditions.invisibilityOf(element));
        setImplicitlyWaitTime(10);
    }

    protected void waitUntilElementContainsText(WebElement element) {
        getWait().until((ExpectedCondition<Boolean>) driver -> !element.getAttribute("value").replaceAll(" ", "").equals(""));
    }
    protected void openNewTab() {
        getDriver().switchTo().newWindow(WindowType.TAB);
    }

    protected void switchTabByPosition(int position) {
        List<String> tabs = new ArrayList<>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(position));
    }

    protected int getTabLength() {
        List<String> tabs = new ArrayList<>(getDriver().getWindowHandles());
        return tabs.size();
    }

    public void refreshPage(){
        getDriver().navigate().refresh();
    }

    public static byte[] takeScreenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    public static byte[] takeScreenshot(WebElement element) {
        return ((TakesScreenshot) element).getScreenshotAs(OutputType.BYTES);
    }
    public void closeCurrentTab() {
        getDriver().close();
        switchTabByPosition(getTabLength()-1);
    }

    public static void quitDriver() {
        driver.quit();
    }


}
