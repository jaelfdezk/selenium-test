package steps.login;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class LoginSteps {
    LoginFunctions loginFunctions= new LoginFunctions();
    LoginAsserts loginAsserts = new LoginAsserts();

    @Given("the user is on login section")
    public void theUserIsOnLoginSection() {
        loginFunctions.visitLoginPage();
    }

    @When("the login enters his credentials")
    public void theLoginEntersHisCredentials() {
        loginFunctions.login("standard_user", "secret_sauce");
    }

    @Then("the app should be display the main section")
    public void theAppShouldBeDisplayTheMainSection() {
        loginAsserts.assertSuccessfullyLogin();
    }

    @Then("the app should not displayed the main section")
    public void theAppShouldNotDisplayedTheMainSection() {
        Assert.fail("La vida sigue");
    }
}
