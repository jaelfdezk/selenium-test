package steps.login;

import org.testng.Assert;
import pages.inventory.InventoryPage;
import support.cucumber.CucumberManager;

public class LoginAsserts {
    private final InventoryPage inventoryPage = new InventoryPage();
    public void assertSuccessfullyLogin(){
        Assert.assertTrue(inventoryPage.isInventorySectionDisplayed());
        CucumberManager.attachScreenshotToReport(inventoryPage.inventoryScreenShot());
        CucumberManager.attachTextToReport("Prueba", "prueba");
    }
}
