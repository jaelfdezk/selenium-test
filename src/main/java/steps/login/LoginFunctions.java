package steps.login;

import pages.login.LoginPage;
import support.cucumber.CucumberManager;

public class LoginFunctions {
    protected LoginPage loginPage = new LoginPage();

    public void visitLoginPage(){
        loginPage.visitLoginPage();
    }

    public void login(String userName, String password){
        loginPage.enterUserName(userName);
        loginPage.enterPassword(password);
        loginPage.clickLoginButton();
        CucumberManager.attachTextToReport("Prueba", "prueba");
    }
}
