package steps.base.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import pages.base.BasePage;
import support.cucumber.CucumberManager;
import support.selenium.DriverManager;
public class Hooks {

    @Before
    public void setUp(Scenario scenario) {
        BasePage.setDriver(DriverManager.getWebDriver());
        new CucumberManager().setScenario(scenario);
    }

    @After(order = 10)
    public void tearDown() {
        BasePage.quitDriver();
    }
}
