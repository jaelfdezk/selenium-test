package support.properties;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class PropertiesManager {
    static Properties properties;
    static{
        try {
            properties = new Properties();
            InputStream input = Files.newInputStream(Paths.get("config.properties"));
            properties.load(input);
        } catch (Exception exception) {
            properties = new Properties();
            properties.setProperty("webDomain", System.getenv("webDomain"));
        }
    }
    public static String getWebDomain() {
        return properties.getProperty("webDomain");
    }
}
